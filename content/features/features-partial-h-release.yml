---
  release:
    content: Release
    copy_media:
      header: Release
      aos_animation: fade-up
      aos_duration: 500
      icon_url: /nuxt-images/icons/devops/release-colour.svg
      subtitle: GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
      text: GitLab helps automate the release and delivery of applications, shortening the delivery lifecycle, streamlining manual processes, and accelerating team velocity. With zero-touch Continuous Delivery (CD) built right into the pipeline, deployments can be automated to multiple environments like staging and production, and the system just knows what to do without being told - even for more advanced patterns like canary deployments. With feature flags, built-in auditing/traceability, and on-demand environments, you'll be able to deliver faster and with more confidence than ever before.
      categories:
        - Continuous Delivery
        - Advanced Deployments
        - Feature Flags
        - Release Evidence
        - Release Orchestration
        - Environment Management
  feature_content:
    products_category:
      products:
        # Continuous Delivery
        - title: Deploy from Chat
          categories:
            - Continuous Delivery
            - ChatOps
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Deploy from one environment (e.g. staging) to any other (e.g. production) from Chat
          link:
            href: https://docs.gitlab.com/ee/integration/slash_commands.html
            text: Documentation
            data_ga_name: Deploy from Chat
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Multi-project pipeline graphs
          categories:
            - Continuous Delivery
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Visualize how pipelines across projects are linked together, including cross project dependencies.
          link:
            href: https://docs.gitlab.com/ee/ci/multi_project_pipelines.html
            text: Documentation
            data_ga_name: Multi-project pipeline graphs
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: Timed and manual incremental rollout deployments
          categories:
            - Continuous Delivery
            - Advanced Deployments
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab can allow you to deploy a new version of your app on Kubernetes starting with just a few pods, and then increase the percentage if everything is working fine. This can be configured to proceed per a schedule or to pause for input to proceed.
          link:
            href: https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production
            text: Documentation
            data_ga_name: Timed and manual incremental rollout deployments
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Canary Deployments
          categories:
            - Continuous Delivery
            - Advanced Deployments
            - Kubernetes Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab Premium can monitor your Canary Deployments when deploying your applications with Kubernetes. Canary Deployments can be configured directly through `.gitlab-ci.yml`, the API, or from the UI of the Deploy Boards.
          link:
            href: https://docs.gitlab.com/ee/user/project/canary_deployments.html
            text: Documentation
            data_ga_name: Canary Deployments
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Pipeline Resource Groups
          categories:
            - Continuous Delivery
            - Release Orchestration
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Allows limiting the concurrency of running jobs to permit only one deployment per resource group at any given time. Resource groups are also supported for parent-child and multi-project pipelines.
          link:
            href: https://docs.gitlab.com/ee/ci/yaml/#resource_group
            text: Documentation
            data_ga_name: Pipeline Resource Groups
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Robust deploy and rollback bundle
          categories:
            - Continuous Delivery
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Encapsulate knowledge of deploying and rolling back into something more than a script, perhaps similar to a k8s operator. Something that knows how to handle failure. e.g. if you’re deploying 7 services and one fails, you can’t just stop, you probably have to rollback the 6 that succeeded, as well as the 7th that failed. (Now, depending on implementation, it still might be a script that triggers some kind of operator). GitLab can deploy and rollback, but only via scripts with limited error handling.
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: Auto Rollback in case of failure
          categories:
            - Continuous Delivery
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: If a critical alert is triggered on production, Auto rollback rolls your deployment back to the last successful deployment.
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Pre-written deploy target mechanisms
          categories:
            - Continuous Delivery
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab Auto DevOps knows how to deploy to Kubernetes. Other vendors have built-in mechanisms to deploy to AWS VMs, Fargate, etc.
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: Load balancer management for Blue/Green deployment
          categories:
            - Continuous Delivery
            - Advanced Deployments
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Blue/green deployment requires switching traffic from one set of servers to another. With GitLab today, you can manage of your load balancer via scripts, but it's not built in as a first-class citizen.
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        # Advanced Deployments
        - title: Complex, simultaneous deployments per environment
          categories:
            - Advanced Deployments
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Canaries, blue/green deploys, and other simultaneous deployment concepts where an environment, like production, would have multiple deployments running at the same time. GitLab has this information, and can even show canary deployments in the deploy board, but in some other places only shows the most recent deployment.
          link:
            href: https://docs.gitlab.com/ee/ci/yaml/#resource_group
            text: Documentation
            data_ga_name: Complex, simultaneous deployments per environment
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Feature Flags
        - title: Feature Flags
          categories:
            - Feature Flags
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: This feature gives you the ability to configure and manage feature flags for your software directly in the product. Simply create a new feature flag, validate it using the simple API instructions in your software, and you have the ability to control the behavior of your software via the feature flag within GitLab itself. Feature Flag strategies can be set per environment . GitLab Feature Flags includes an API for interacting with them.
          link:
            href: https://docs.gitlab.com/ee/operations/feature_flags.html
            text: Documentation
            data_ga_name: Feature Flags
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Feature Flag List view
          categories:
            - Feature Flags
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: This feature gives you the ability to view all the feature flags configured in a project. You can toggle the flags on or off directly from this page, and view all the associated information for a flag. This includes the strategies linked to the flag, the number or percent of users affected, and the environments.
          link:
            href: https://docs.gitlab.com/ee/operations/feature_flags.html
            text: Documentation
            data_ga_name: Feature Flag List view
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Percent of Users Strategy for Feature Flags
          categories:
            - Feature Flags
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: You can select "Percent of Users" as a rollout strategy for your feature flags. This allows percentages to be set individually for each environment and each flag. When "Percent of Users" is configured and the flag is enabled, the feature will be shown to the configured percentage of logged-in users. This allows you to do controlled rollouts and monitor the behavior of the target environment to ensure the results are as expected.
          link:
            href: https://docs.gitlab.com/ee/operations/feature_flags.html#rollout-strategy
            text: Documentation
            data_ga_name: Percent of Users Strategy for Feature Flags
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Flexible Rollout Strategy for Feature Flags
          categories:
            - Feature Flags
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: You can define the stickiness of the rollout strategy. This can be based on the session ID or user ID, or random (no stickiness). This gives you more control over the rollout and also opens the option for supporting stickiness for anonymous users.
          link:
            href: https://docs.gitlab.com/ee/operations/feature_flags.html#percent-rollout
            text: Documentation
            data_ga_name: Flexible Rollout Strategy for Feature Flags
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: UserID Rollout Strategy for Feature Flags
          categories:
            - Feature Flags
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: You can choose "User ID" as a rollout strategy for your feature flags. The User ID strategy allows you to specify a comma-separated list of User IDs and then toggle a feature flag only for the specified users. This can allow you to target testing features with specific cohorts or segments of your userbase.
          link:
            href: https://docs.gitlab.com/ee/operations/feature_flags.html#target-users
            text: Documentation
            data_ga_name: UserID Rollout Strategy for Feature Flags
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: User List Strategy for Feature Flags
          categories:
            - Feature Flags
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: You can choose "User List" as a rollout strategy for your feature flags. User lists can be reused for multiple feature flags while allowing you to manage them in a single location. You can create Feature Flag user lists from the API, and edit or delete them from the API or UI.
          link:
            href: https://docs.gitlab.com/ee/operations/feature_flags.html#user-list
            text: Documentation
            data_ga_name: User List Strategy for Feature Flags
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Associate Feature Flags with the issue(s) that is related to them
          categories:
            - Feature Flags
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: You can create a link from the issue that introduced the Feature Flag to the Feature Flag itself. That relationship is visible in the Feature Flag details. Feature Flags also support Markdown and can be referenced from any issue.
          link:
            href: https://docs.gitlab.com/ee/api/features.html
            text: Documentation
            data_ga_name: Associate Feature Flags with the issue(s) that is related to them
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: Set multiple strategies per environment
          categories:
            - Feature Flags
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: You can define multiple strategies independent of environments via API and/or UI.
          link:
            href: https://docs.gitlab.com/ee/api/features.html
            text: Documentation
            data_ga_name: Set multiple strategies per environment
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Release Evidence
        - title: Release Evidence
          categories:
            - Release Evidence
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Snapshots of the Release's metadata at time of creation and completion in JSON format leveraged as a chain of custody to support review processes, as conducted in an audit.
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/#release-evidence
            text: Documentation
            data_ga_name: Release Evidence
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Release Orchestration
        - title: Keep track of releases using GitLab Releases
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab's Releases feature allow you to track deliverables in your project. Consider them a snapshot in time of the source, build output, and other metadata or artifacts associated with a released version of your code, and receive notifications when new releases are available for projects you track, including for guests of your project.
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/#release-evidence
            text: Documentation
            data_ga_name: Keep track of releases using GitLab Releases
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Associate Releases with Milestones
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: The way many teams use GitLab, ourselves included, is to have a milestone for the release that everything tracks to. Some teams may also have more than one sprint that makes up a release. With GitLab you can associate a milestone (or more) to a release; this will populate the release page with issues and merge requests included in the release(s).
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/#releases-associated-with-milestones
            text: Documentation
            data_ga_name: Associate Releases with Milestones
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Create a release from the UI
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: You can create a release by associating it to a new or existing tag. This functionality is supported both in the UI and API. With this feature, users have more flexibility when planning releases and can associate tags to releases.
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/#create-a-release
            text: Documentation
            data_ga_name: Create a release from the UI
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Environments history
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Environments history allows you to see what is currently being deployed on your servers, and to access a detailed view for all the past deployments. From this list you can also re-deploy the current version, or even rollback an old stable one in case something went wrong.
          link:
            href: https://docs.gitlab.com/ee/ci/environments/index.html#viewing-the-deployment-history-of-an-environment
            text: Documentation
            data_ga_name: Environments history
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Environment-specific variables
          categories:
            - Release Orchestation
            - Environment Management
            - Kubernetes Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Limit the environment scope of a variable by defining which environments it can be available for.
          link:
            href: https://docs.gitlab.com/ee/ci/variables/#limiting-environment-scopes-of-environment-variables
            text: Documentation
            data_ga_name: Environment-specific variables
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Group-level release analytics
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: View how many releases belong to this group and what percent of the projects are associated with releases at the group level
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/#release-metrics
            text: Documentation
            data_ga_name: Group-level release analytics
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Manage access to protected environments from the API
          categories:
            - Release Orchestation
            - Environment Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab offers users the ultimate flexibility of setting up and configuring Environments via the API or UI. We also support Maintainer's control of access to Protected Environments via API.
          link:
            href: https://docs.gitlab.com/ee/api/protected_environments.html
            text: Documentation
            data_ga_name: Manage access to protected environments from the API
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: Release Progress view
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: The progress percent complete of issues within a release.
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/
            text: Documentation
            data_ga_name: Release Progress view
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Deploy Freeze
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Easily lock changes to target environments during defined time periods.
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/#set-a-deploy-freeze
            text: Documentation
            data_ga_name: Deploy Freeze
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Link runbooks to a Release
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Runbooks can contain a series of steps related to executing a successful release. Link these plans to the Release page in GitLab to coordinate activities across teams, inside and outside of GitLab.
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/index.html#links
            text: Documentation
            data_ga_name: Link runbooks to a Release
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Create a release directly from the .gitlab-ci.yml via the release CLI
          categories:
            - Release Orchestation
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab supports creation of a release directly from the .gitlab-ci.yml via the release CLI. The name and description can be configured directly in the .gitlab-ci.yml or read from another file in the repository.
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/index.html#release-command-line
            text: Documentation
            data_ga_name: Create a release directly from the .gitlab-ci.yml via the release CLI
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Environment Management
        - title: Protected Environments
          categories:
            - Environment Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Specify which person, group, or account is allowed to deploy to a given environment, allowing further protection and safety of sensitive environments.
          link:
            href: https://docs.gitlab.com/ee/ci/environments/protected_environments.html
            text: Documentation
            data_ga_name: Protected Environments
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: Environments Dashboard
          categories:
            - Environment Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Visualize cross-project environments, track change flow from development to production, track pipeline status and diagnose issues from a single dashboard.
          link:
            href: https://docs.gitlab.com/ee/ci/environments/environments_dashboard.html
            text: Documentation
            data_ga_name: Environments Dashboard
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: View alerts on the Environments page
          categories:
            - Environment Management
            - Continuous Delivery
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Seeing triggered alerts alongside the status of your environments enable you to take immediate action to remedy the situation.
          link:
            href: https://docs.gitlab.com/ee/ci/environments/#working-with-environments
            text: Documentation
            data_ga_name: View alerts on the Environments page
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Environments and deployments
          categories:
            - Environment Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab CI is capable of not only testing or building your projects, but also deploying them in your infrastructure, with the added benefit of giving you a way to track your deployments. Environments are like tags for your CI jobs, describing where code gets deployed.
          link:
            href: https://docs.gitlab.com/ee/ci/environments/index.html
            text: Documentation
            data_ga_name: Environments and deployments
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Per-environment permissions
          categories:
            - Environment Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Developers and QA can deploy to their own environments on demand while production stays locked down. Build engineers and ops teams spend less time servicing deploy requests, and can gate what goes into production.
          link:
            href: https://docs.gitlab.com/ee/user/project/protected_branches.html
            text: Documentation
            data_ga_name: Per-environment permissions
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Environment type
          categories:
            - Environment Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: "When defining an environment in `.gitlab-ci.yml`, in addition to the environment `name`, you can define the environment `type`, which can be one of: `production`, `staging`, `testing`, `development`, or `other`."
          link:
            href: https://docs.gitlab.com/ee/ci/environments/#deployment-tier-of-environments
            text: Documentation
            data_ga_name: Environment type
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        # RELEASE - MISSING FEATURES
        # Continuous Delivery - Missing
        - title: Active confirmation of what is running in production
          categories:
            - Continuous Delivery
            - Environment Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab knows how to deploy to production (and other environments), but then trusts the result. Other vendors actively query the target environment to know what is running.

