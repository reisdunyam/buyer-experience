---
  title: GitLab Ultimate for IBM
  description: Accelerate innovation with the DevOps solution. Simplify how software is developed, delivered, and managed with technologies from GitLab and IBM.
  components:
    - name: 'partners-call-to-action'
      data:
          title: GitLab Ultimate for IBM
          inverted: true
          body:
            - header:
              text: Accelerate innovation with the DevOps solution. Simplify how software is developed, delivered, and managed with technologies from GitLab and IBM.
            - header: Discover efficiency, deliver results
              text: Increase productivity with agile collaboration and optimize delivery with data-driven automation. GitLab Ultimate provides the DevOps platform for delivering better hybrid cloud software faster with IBM.
          primary_button:
            url: https://learn.gitlab.com/451-ibm-gitlab-aiops-devops/451-ibm-oem
            text: Read the 451 report
            data_ga_name: read the report
          secondary_button:
            url: https://www.ibm.com/products/gitlab-ultimate
            text: Learn more
            data_ga_name: learn mores
          image:
              image_url: /nuxt-images/partners/ibm/ibm.png
              alt: IBM logo
    - name: 'copy-media'
      data:
        block:
            - header: Modernize applications with GitLab Ultimate on IBM Z
              text: |
                Now every team in your organization can collaboratively plan, build, secure and deploy software to drive business outcomes faster with complete transparency, consistency, and traceability. Maximize return on software development and operations by delivering faster and more efficiently.
              image:
                image_url: /nuxt-images/partners/ibm/featured-partner-ibm-1.svg
                alt: ""
              link_href: /blog/2021/09/10/adopt-agile-and-devops-for-ibm-z/
              link_text: The benefits of DevOps practices for IBM Z
              secondary_link_href: https://www.ibm.com/products/gitlab-ultimate/zos
              secondary_link_text: Learn more
    - name: 'partners-feature-showcase'
      data:
        header: Automate your enterprise workloads to deliver better applications faster
        image_url: /nuxt-images/partners/ibm/featured-partner-ibm-2.svg
        text: For organizations to rapidly shift workloads to the cloud, they need a secure, portable method without vendor lock-in. GitLab Ultimate for IBM Cloud Paks is designed to help project teams that want to deploy an application to different systems located on environments such as IBM Cloud, IBM Z, or bare metal servers, as a single, comprehensive hybrid cloud solution.
        cards:
          - header: Productive pipelines
            text: GitLab Ultimate for IBM offers a standardized pipeline that builds on the CI/CD, application security testing, source control, and single-user experience of GitLab with out-of-the-box extensions for service virtualization, integration testing, release orchestration, and scalable governance.
          - header: Integrated DevSecOps
            text: Together, IBM and GitLab help clients streamline collaboration to improve productivity and accelerate innovation in the environment of their choosing. Discover an integrated DevSecOps approach with extensions for other tools.
          - header: Unparalleled automation
            text: GitLab Ultimate is integrated with IBM Cloud Paks, IBM Watson AIOps, IBM Z, and IBM Power to help teams become more agile and efficient. With GitLab Ultimate for IBM, companies can automate work across business users, developers, and IT teams.
    - name: 'benefits'
      data:
        header: Get started with GitLab and IBM
        full_background: true
        text_align: 'left'
        cards_per_row: 2
        description: From centralized management to cost reduction – GitLab for IBM Cloud Paks, Watson, IBM Z, and IBM Power offers an end-to-end solution that streamlines hybrid software delivery across multiple pipelines.
        benefits:
          - icon: /nuxt-images/icons/icon-1.svg
            title: GitLab + IBM Cloud Paks
            description: Leverage AI-powered software built on Red Hat OpenShift for hybrid clouds. With GitLab and IBM Cloud Paks, fully implement intelligent workflows to accelerate your business’s digital transformation.
            link:
              text: Learn more
              url: https://www.ibm.com/products/gitlab-ultimate
          - icon: /nuxt-images/icons/icon-2.svg
            title: GitLab + IBM Cloud Paks
            description: Leverage AI-powered software built on Red Hat OpenShift for hybrid clouds. With GitLab and IBM Cloud Paks, fully implement intelligent workflows to accelerate your business’s digital transformation.
            link:
              text: Learn more
              url: https://www.ibm.com/products/gitlab-ultimate
          - icon: /nuxt-images/icons/icon-3.svg
            title: GitLab + IBM Cloud Paks
            description: Leverage AI-powered software built on Red Hat OpenShift for hybrid clouds. With GitLab and IBM Cloud Paks, fully implement intelligent workflows to accelerate your business’s digital transformation.
            link:
              text: Learn more
              url: https://www.ibm.com/products/gitlab-ultimate
          - icon: /nuxt-images/icons/icon-3.svg
            title: GitLab + IBM Cloud Paks
            description: Leverage AI-powered software built on Red Hat OpenShift for hybrid clouds. With GitLab and IBM Cloud Paks, fully implement intelligent workflows to accelerate your business’s digital transformation.
            link:
              text: Learn more
              url: https://www.ibm.com/products/gitlab-ultimate
    - name: 'pull-quote'
      data:
        quote: Key use cases for the new product include efficient and secure automation of software development, delivery, and management, and the use of AI to accelerate DevOps by making processes and responses more intelligent.
        source: JAY LYMAN, 451 RESEARCH (2021)
        link_text: ''
        shadow: true
    - name: copy-resources
      data:
        title: Discover the benefits of GitLab on IBM
        block:
          - video:
              title: 'Opening Keynote: The Power of GitLab - Sid Sijbrandij'
              video_url: https://www.youtube.com/embed/xn_WP4K9dl8?enablesjsapi=1
              label: Gitlab commit virtual 2020
            resources:
              webcast:
                header: Videos
                links:
                  - text: 'Customer Success Webinar: OneMain Financial: GitLab and DBB on IBM Z '
                    link: https://event.on24.com/eventRegistration/EventLobbyServlet?target=reg20.jsp&referrer=https%3A%2F%2Fnotes.services.box.com%2F&eventid=3177900&sessionid=1&key=8A3DC08B29BCA630D9B9AB2050D97333&regTag=&V2=false&sourcepage=register
                  - text: How to transform legacy IT into a z/OS DevOps team
                    link: https://www.youtube.com/watch?v=AcTw9m_5fgs
              whitepaper:
                header: Whitepapers
                links:
                  - text: An overview of GitLab Ultimate for IBM Cloud Paks
                    link: https://community.ibm.com/HigherLogic/System/DownloadDocumentFile.ashx?DocumentFileKey=58a6cbe3-8cb1-2683-8ee6-913879f285bd&forceDialog=0
                  - text: 'IBM GitLab Utlimate and IBM Z: Solution Brief '
                    link: https://www.ibm.com/downloads/cas/0AD4LDAO
              blog:
                header: Blogs
                links:
                  - text: Accelerating Modernization on IBM Power with GitLab
                    link: https://www.ibm.com/blogs/systems/accelerating-modernization-on-ibm-power-with-open-source-software-and-ci-cd-tooling/
                  - text: GitLab Runner Operator on IBM Power
                    link: https://sneha-kanekar.medium.com/gitlab-runner-operator-on-power-659114a43f5a
                  - text: GitLab's journey on Linux on IBM Z and Red Hat OpenShift
                    link: /blog/2020/09/17/gitlab-and-workloads-on-ibm-z-and-red-hat-openshift/
                  - text: 'IBM GitLab Utlimate and IBM Z: Podcast with Rosalind Radcliffe '
                    link: https://community.ibm.com/community/user/wasdevops/blogs/laurel-dickson-bull1/2021/03/22/ibm-gitlab-utlimate-for-zos-podcast-with-rosalind
                  - text: Expanding our DevOps portfolio with GitLab Ultimate for IBM Cloud Paks
                    link: https://www.ibm.com/blogs/systems/expanding-our-devops-portfolio-with-gitlab-ultimate-for-ibm-cloud-paks/
                  - text: The Importance of DevOps Automation
                    link: https://www.ibm.com/cloud/blog/importance-of-devops-automation
                  - text: Automating the Enterprise One Idea at a time
                    link: https://www.ibm.com/cloud/blog/announcements/automating-the-enterprise-one-idea-at-a-time-ibm-and-gitlab
                  - text: What does IBM partnership with GitLab mean for Rational test
                    link: https://community.ibm.com/community/user/wasdevops/blogs/suhas-kashyap1/2021/01/14/what-does-ibm-partnership-with-gitlab-mean-for-rat
                  - text: GitLab and UrbanCode
                    link: https://community.ibm.com/community/user/wasdevops/viewdocument/deploy-complex-applications-with-gi?CommunityKey=0ab505af-8e12-4199-843b-0dbbb3848f0e
