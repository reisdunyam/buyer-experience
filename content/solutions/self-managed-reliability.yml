---
  title: Self-managed reliability
  description: Ensure disaster recovery, high availability and load balancing of your self-managed deployment.
  components:
    - name: 'solutions-hero'
      data:
        title: Self-managed reliability
        subtitle: Disaster recovery, high availability and load balancing for self-managed deployments
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
        # TODO: Change for actual image
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitlab for self-managed reliability"
    - name: 'copy-media'
      data:
        block:
          - header:
            text: GitLab offers customers a choice of two deployment options - SaaS and Self-managed. For customers that choose to host and manage their GitLab instance themselves, the premium tier and above offer disaster recovery, high availability and load balancing of your self-managed deployment.
            image:
              image_url: /nuxt-images/solutions/self-managed-reliability-geo.png
              alt: ""
    - name: 'solutions-feature-list'
      data:
        title: 'GitLab Self Managed Reliability.'
        icon: /nuxt-images/enterprise/gitlab-enterprise-icon-tanuki.svg
        features:
          - title: High availability and Disaster Recovery
            description: |
              *   Failover to another data center in minutes with [disaster recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/){data-ga-name="disaster recovery" data-ga-location="body"}

              *   Replicated Git storage with automatic failover

              *   [Maintenance mode](https://docs.gitlab.com/ee/administration/maintenance_mode/){data-ga-name="maintenance mode" data-ga-location="body"} for minimum disruption to end users

              *   [Log forwarding](https://docs.gitlab.com/omnibus/settings/logs.html#udp-log-forwarding){data-ga-name="log forwarding" data-ga-location="body"} to a central system
            icon: /nuxt-images/icons/slp-cicd.svg
          - title: Support distributed environments
            description: |
              *   Distributed cloning with [GitLab geo](/solutions/geo/){data-ga-name="geo" data-ga-location="body"} to reduce time to clone and fetch large repos

              *   [Container registry](https://docs.gitlab.com/ee/administration/geo/){data-ga-name="container registry" data-ga-location="body"} replication to improve collaboration for distributed teams
            icon: /nuxt-images/icons/Icon-code.svg
          - title:  Scaling
            icon: /nuxt-images/icons/icon-gear.svg
            description: |
              *   Support for [scaled architectures](/support/#definition-of-scaled-architecture/){data-ga-name="scaled architectures" data-ga-location="body"} to manage increasing demand and provide redundancy
    - name: 'solutions-carousel-videos'
      data:
        videos:
          - title: The best solution at scale
            photourl: /nuxt-images/video-carousels/self-managed-reliability/best-solution-scale.jpeg
            carousel_identifier:
              - Unfiltered
            video_link: https://www.youtube.com/embed/TFE6unteEls?enablesjsapi=1
          - title: GitLab Admin mode
            photourl: /nuxt-images/video-carousels/self-managed-reliability/admin-mode.jpeg
            carousel_identifier:
              - Unfiltered
            video_link: https://www.youtube.com/embed/i1AwQIT7JfI?enablesjsapi=1
          - title: GitLab Geo tutorial
            photourl: /nuxt-images/video-carousels/self-managed-reliability/gitlab-geo-thumbnail.jpeg
            carousel_identifier:
              - Unfiltered
            video_link: https://www.youtube.com/embed/h3Dwhv8JgPU?enablesjsapi=1
